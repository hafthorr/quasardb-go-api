#ifndef QDB_API_SET_H
#define QDB_API_SET_H

//! \file hset.h
//! \defgroup hset Distributed hash set operations
//! \warning All functions declared in this file are experimental.

/*
 *
 * Official quasardb C API 2.0.0
 * c926a24 2017-01-30 11:44:33 +0100
 *
 * Copyright (c) 2009-2017, quasardb SAS
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of quasardb nor the names of its contributors may
 *      be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY QUASARDB AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "client.h"

#ifdef __cplusplus
extern "C" {
#endif

//! \ingroup hset
//! \brief Inserts an element into the distributed hash set of the specified
//! name.
//!
//! A distributed hash set is transparently distributed over the nodes of the
//! cluster. It does not have
//! any limit on the number of entries.
//!
//! If the set does not exist, it will be created.
//!
//! If the entry is already present in the hset, the function will return
//! `qdb_e_element_already_exists`.
//!
//! \param handle A valid handle previously initialized by ::qdb_open or
//! ::qdb_open_tcp.
//! \param alias A pointer to a null-terminated UTF-8 string representing the
//! alias of the hset.
//! \param content A pointer to a buffer holding the content to insert into the
//! set.
//! \param content_length The length of the buffer to insert into the set
//! \return A ::qdb_error_t code indicating success or failure.
//! \see qdb_hset_erase, qdb_hset_contains
//! \warning This function is experimental.
QDB_API_LINKAGE qdb_error_t qdb_hset_insert(qdb_handle_t handle,
                                            const char * alias,
                                            const void * content,
                                            qdb_size_t content_length);

//! \ingroup hset
//! \brief Removes an element from the distributed hash set.
//!
//! A distributed hash set is transparently distributed over the nodes of the
//! cluster. It does not have
//! any limit on the number of entries.
//!
//! The content must match bit for bit the inserted content to be considered
//! equal.
//!
//! It is an error to attempt to remove an element from a non-existing set.
//!
//! If you remove the last element from the set, the set will be empty but not
//! removed from the database.
//!
//! If the entry is not present in the hset, the function will return
//! `qdb_e_element_not_found`.
//!
//! \param handle A valid handle previously initialized by ::qdb_open or
//! ::qdb_open_tcp.
//! \param alias A pointer to a null-terminated UTF-8 string representing the
//! alias of the hset.
//! \param content A pointer to a buffer holding the content to remove from the
//! set.
//! \param content_length The length of the buffer to remove from the set
//! \return A ::qdb_error_t code indicating success or failure.
//! \see qdb_hset_insert, qdb_hset_contains
//! \warning This function is experimental.
QDB_API_LINKAGE qdb_error_t qdb_hset_erase(qdb_handle_t handle,
                                           const char * alias,
                                           const void * content,
                                           qdb_size_t content_length);

//! \ingroup hset
//! \brief Tests for the existence of an element in the distributed hash set.
//!
//! A distributed hash set is transparently distributed over the nodes of the
//! cluster. It does not have
//! any limit on the number of entries.
//!
//! The content must match bit for bit the inserted content to be considered
//! equal.
//!
//! It is an error to attempt to look for an element in a non-existing set.
//!
//! If the entry is not present in the hset, the function will return
//! `qdb_e_element_not_found`.
//!
//! \param handle A valid handle previously initialized by ::qdb_open or
//! ::qdb_open_tcp.
//! \param alias A pointer to a null-terminated UTF-8 string representing the
//! alias of the hset.
//! \param content A pointer to a buffer holding the content to search for in
//! the set.
//! \param content_length The length of the buffer to remove from the set
//! \return A ::qdb_error_t code indicating success or failure.
//! \see qdb_hset_insert, qdb_hset_contains
//! \warning This function is experimental.
QDB_API_LINKAGE qdb_error_t qdb_hset_contains(qdb_handle_t handle,
                                              const char * alias,
                                              const void * content,
                                              qdb_size_t content_length);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
