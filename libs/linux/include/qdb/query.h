#ifndef QDB_API_QUERY_H
#define QDB_API_QUERY_H

//! \file query.h
//! \defgroup query Query related functions

/*
 *
 * Official quasardb C API 2.1.0
 * 1a277ed 2017-08-04 15:38:44 +0000
 *
 * Copyright (c) 2009-2017, quasardb SAS
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of quasardb nor the names of its contributors may
 *      be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY QUASARDB AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "client.h"

#ifdef __cplusplus
extern "C" {
#endif

//! \ingroup query
//! \brief Retrieves all entries' aliases that match the specified query.
//!
//! For the complete grammar, please refer to the documentation.
//!
//! Queries are transactional.
//!
//! The complexity of this function is dependent on the complexity of the query.
//!
//! \param handle A valid handle previously initialized by ::qdb_open or
//!               ::qdb_open_tcp.
//! \param query A pointer to a null-terminated UTF-8 string representing the
//!            query to perform.
//! \param aliases A pointer to an array of null-terminated UTF-8 string
//!                that will list the aliases of the entries matching the query.
//! \param alias_count A pointer to an integer that will receive the number of
//!                    returned aliases.
//! \return A ::qdb_error_t code indicating success or failure.
//!
//! \see qdb_release
QDB_API_LINKAGE qdb_error_t qdb_query(qdb_handle_t handle,
                                      const char * query,
                                      const char *** aliases,
                                      size_t * alias_count);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* QDB_API_QUERY_H */
