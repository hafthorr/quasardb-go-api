#ifndef QDB_API_TS_H
#define QDB_API_TS_H

//! \file ts.h
//! \defgroup ts Time series operations
//! \warning Time series are under development. Performance and compatibility
//! are not guaranteed.

/*
 *
 * Official quasardb C API 2.1.0
 * 1a277ed 2017-08-04 15:38:44 +0000
 *
 * Copyright (c) 2009-2017, quasardb SAS
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of quasardb nor the names of its contributors may
 *      be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY QUASARDB AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "client.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(push)
#pragma pack(16)

//! \ingroup ts
//! \brief A timestamped data with a numeric value.
typedef struct
{
    //! \brief Timestamp.
    qdb_timespec_t timestamp;
    //! \brief Numeric value.
    double value;
} qdb_ts_double_point;

//! \ingroup ts
//! \brief A timestamped data with a binary content.
typedef struct
{
    //! \copydoc qdb_ts_double_point::timestamp
    qdb_timespec_t timestamp;
    //! Pointer to data.
    const void * content;
    //! Length, in bytes, of the data pointed to by \ref content.
    qdb_size_t content_length;
} qdb_ts_blob_point;

//! \ingroup ts
//! \brief Time interval.
typedef struct
{
    //! Beginning of the interval, inclusive.
    qdb_timespec_t begin;
    //! End of the interval, exclusive.
    qdb_timespec_t end;
} qdb_ts_range_t;

//! \ingroup ts
//! \brief Types of aggregations that can be computed on a time series.
typedef enum qdb_ts_aggregation_type {
    qdb_agg_first = 0,           //!< The first (earliest) data point.
    qdb_agg_last = 1,            //!< The last (latest) data point.
    qdb_agg_min = 2,             //!< The data point with the smallest value.
    qdb_agg_max = 3,             //!< The data point with the largest value.
    qdb_agg_arithmetic_mean = 4, //!< The arithmetic mean (average).
    qdb_agg_harmonic_mean = 5,   //!< The harmonic mean.
    qdb_agg_geometric_mean = 6,  //!< The geometric mean.
    qdb_agg_quadratic_mean = 7,  //!< The quadratic mean (root mean squaer).
    qdb_agg_count = 8,           //!< The number of data points.
    qdb_agg_sum = 9,             //!< The sum of values.
    qdb_agg_sum_of_squares = 10, //!< The sum of squares of values.
    qdb_agg_spread = 11,
    //!< The difference between the maximum value and the minimum value.
    qdb_agg_sample_variance = 12,     //!< The sample variance.
    qdb_agg_sample_stddev = 13,       //!< The sample standard deviation.
    qdb_agg_population_variance = 14, //!< The population variance.
    qdb_agg_population_stddev = 15,   //!< The population standard deviation.
    qdb_agg_abs_min = 16,  //!< The value with the smallest absolute value.
    qdb_agg_abs_max = 17,  //!< The value with the largest absolute value.
    qdb_agg_product = 18,  //!< The product.
    qdb_agg_skewness = 19, //!< The skewness (shape parameter).
    qdb_agg_kurtosis = 20  //!< The kurtosis (shape parameter).
} qdb_ts_aggregation_type_t;

//! \ingroup ts
//! \brief Aggregation input and result for columns of numeric values.
typedef struct qdb_ts_double_aggregation
{
    //! The type of the aggregation to perform.
    qdb_ts_aggregation_type_t type;
    //! The time interval on which the aggregation should be executed.
    qdb_ts_range_t range;
    //! \brief If applicable, the number of datapoints on which aggregation has
    //! been computed.
    qdb_size_t count;
    //! \brief The result of the aggregation.
    qdb_ts_double_point result;
} qdb_ts_double_aggregation_t;

//! \ingroup ts
//! \brief Aggregation input and result for columns of blobs.
typedef struct qdb_ts_blob_aggregation
{
    //! \copydoc qdb_ts_double_aggregation_t::type
    qdb_ts_aggregation_type_t type;
    //! \copydoc qdb_ts_double_aggregation_t::range
    qdb_ts_range_t range;
    //! \copydoc qdb_ts_double_aggregation_t::count
    qdb_size_t count;
    //! \copydoc qdb_ts_double_aggregation_t::result
    qdb_ts_blob_point result;
} qdb_ts_blob_aggregation_t;

//! \ingroup ts
//! \brief Types of time series columns.
typedef enum qdb_ts_column_type {
    qdb_ts_column_uninitialized = -1,
    qdb_ts_column_double = 0, //!< Column of numeric values.
    qdb_ts_column_blob = 1    //!< Column of binary data.
} qdb_ts_column_type_t;

//! \ingroup ts
//! \brief Description of a time series column.
typedef struct qdb_ts_column_info
{
    //! \brief A pointer to a null-terminated UTF-8 string representing the name
    //! of the column.
    const char * name;
    //! The type of the column.
    qdb_ts_column_type_t type;
} qdb_ts_column_info_t;

#pragma pack(pop)

//! \ingroup ts
//! \brief Creates a time series.
//!
//! \param handle A valid handle previously initialized by ::qdb_open or
//!               ::qdb_open_tcp.
//! \param alias A pointer to a null-terminated UTF-8 string representing the
//!              alias of the time series.
//! \param columns A pointer to an array of column descriptions that are to be
//!                added to the time series.
//! \param column_count The number of columns to add.
QDB_API_LINKAGE qdb_error_t qdb_ts_create(qdb_handle_t handle,
                                          const char * alias,
                                          const qdb_ts_column_info_t * columns,
                                          qdb_size_t column_count);

QDB_API_LINKAGE qdb_error_t
qdb_ts_insert_columns(qdb_handle_t handle,
                      const char * alias,
                      const qdb_ts_column_info_t * columns,
                      qdb_size_t column_count);

//! \ingroup ts
//! \brief Returns all the columns of a time series.
//!
//! \param handle A valid handle previously initialized by ::qdb_open or
//!               ::qdb_open_tcp.
//! \param alias A pointer to a null-terminated UTF-8 string representing the
//!              alias of the time series.
//! \param columns A pointer to an array that will contain descriptions of
//!                columns present in the time series.
//! \param column_count A pointer to an integer that will receive the number
//!                     of columns.
//! \return A ::qdb_error_t code indicating success or failure.
//! \warning This function is still under development. Performance and
//! compatibility are not guaranteed.
QDB_API_LINKAGE qdb_error_t qdb_ts_list_columns(qdb_handle_t handle,
                                                const char * alias,
                                                qdb_ts_column_info_t ** columns,
                                                qdb_size_t * column_count);

//! \ingroup ts
//! \brief Inserts points in a time series.
//!
//! Time series are distributed across the cluster and support efficient
//! insertion anywhere within the time series as well as efficient lookup
//! based on time.
//!
//! \param handle A valid handle previously initialized by ::qdb_open or
//!               ::qdb_open_tcp.
//! \param alias A pointer to a null-terminated UTF-8 string representing the
//!              alias of the time series.
//! \param column A pointer to a null-terminated UTF-8 string representing the
//!               name of the column to work on.
//! \param values A pointer to an array of data points to be inserted.
//! \param count The number of data points to insert.
//! \return A ::qdb_error_t code indicating success or failure.
//! \warning This function is still under development. Performance and
//! compatibility are not guaranteed.
QDB_API_LINKAGE qdb_error_t
qdb_ts_double_insert(qdb_handle_t handle,
                     const char * alias,
                     const char * column,
                     const qdb_ts_double_point * values,
                     qdb_size_t count);

//! \ingroup ts
//! \brief Retrieves doubles in the specified range of the time series column.
//!
//! It is an error to call this function on a non existing time-series.
//!
//! \param handle A valid handle previously initialized by ::qdb_open or
//!               ::qdb_open_tcp.
//! \param alias A pointer to a null-terminated UTF-8 string representing the
//!              alias of the time series.
//! \param column A pointer to a null-terminated UTF-8 string representing the
//!               name of the column to work on.
//! \param ranges A pointer to an array of ranges (intervals) for which data
//!               should be retrieved.
//! \param range_count The number of ranges.
//! \param points A pointer to an array that will contain data points
//!               from all given ranges.
//! \param point_count A pointer to an integer that will receive the number of
//!                    returned points.
//! \return A ::qdb_error_t code indicating success or failure.
//! \warning This function is still under development. Performance and
//! compatibility are not guaranteed.
//! \see ::qdb_release
QDB_API_LINKAGE qdb_error_t
qdb_ts_double_get_ranges(qdb_handle_t handle,
                         const char * alias,
                         const char * column,
                         const qdb_ts_range_t * ranges,
                         qdb_size_t range_count,
                         qdb_ts_double_point ** points,
                         qdb_size_t * point_count);

//! \ingroup ts
//! \brief Aggregate a sub-part of the double column of the time series.
//!
//! It is an error to call this function on a non existing time-series.
//!
//! \param handle A valid handle previously initialized by ::qdb_open or
//!               ::qdb_open_tcp.
//! \param alias A pointer to a null-terminated UTF-8 string representing the
//!              alias of the time series.
//! \param column A pointer to a null-terminated UTF-8 string representing the
//!               name of the column to work on.
//! \param aggregations A pointer to an array of structures representing the
//!                     aggregations to compute and their results.
//! \param aggregation_count The number of elements in the aggregations array.
//! \return A ::qdb_error_t code indicating success or failure.
//! \see qdb_ts_aggregation_type_t
//! \warning This function is still under development. Performance and
//! compatibility are not guaranteed.
QDB_API_LINKAGE qdb_error_t
qdb_ts_double_aggregate(qdb_handle_t handle,
                        const char * alias,
                        const char * column,
                        qdb_ts_double_aggregation_t * aggregations,
                        qdb_size_t aggregation_count);

//! \ingroup ts
//! \brief Aggregate a sub-part of a blob column of the time series.
//!
//! It is an error to call this function on a non existing time-series.
//!
//! \param handle A valid handle previously initialized by ::qdb_open or
//!               ::qdb_open_tcp.
//! \param alias A pointer to a null-terminated UTF-8 string representing the
//!              alias of the time series.
//! \param column A pointer to a null-terminated UTF-8 string representing the
//!               name of the column to work on.
//! \param aggregations A pointer to an array of structures representing the
//!                     aggregations to compute and their results.
//! \param aggregation_count The number of elements in the aggregations array.
//! \return A ::qdb_error_t code indicating success or failure.
//! \see qdb_ts_aggregation_type_t
//! \see ::qdb_release
//! \warning This function is still under development. Performance and
//! compatibility are not guaranteed.
QDB_API_LINKAGE qdb_error_t
qdb_ts_blob_aggregate(qdb_handle_t handle,
                      const char * alias,
                      const char * column,
                      qdb_ts_blob_aggregation_t * aggregations,
                      qdb_size_t aggregation_count);

//! \ingroup ts
//! \brief Inserts blob in a time series column.
//!
//! It is an error to call this function on a non existing time-series.
//!
//! \param handle A valid handle previously initialized by ::qdb_open or
//!               ::qdb_open_tcp.
//! \param alias A pointer to a null-terminated UTF-8 string representing the
//!              alias of the time series.
//! \param column A pointer to a null-terminated UTF-8 string representing the
//!               name of the column to work on.
//! \param values A pointer to an array of data points to be inserted.
//! \param count The number of data points to insert.
//! \return A ::qdb_error_t code indicating success or failure.
//! \warning This function is still under development. Performance and
//! compatibility are not guaranteed.
QDB_API_LINKAGE qdb_error_t qdb_ts_blob_insert(qdb_handle_t handle,
                                               const char * alias,
                                               const char * column,
                                               const qdb_ts_blob_point * values,
                                               qdb_size_t count);
//! \ingroup ts
//! \brief Retrieves blobs in the specified range of the time series column.
//!
//! It is an error to call this function on a non existing time-series.
//!
//! \param handle A valid handle previously initialized by ::qdb_open or
//!               ::qdb_open_tcp.
//! \param alias A pointer to a null-terminated UTF-8 string representing the
//!              alias of the time series.
//! \param column A pointer to a null-terminated UTF-8 string representing the
//!               name of the column to work on.
//! \param ranges A pointer to an array of ranges (intervals) for which data
//!               should be retrieved.
//! \param range_count The number of ranges.
//! \param points A pointer to an array that will contain data points
//!               from all given ranges.
//! \param point_count A pointer to an integer that will receive the number of
//!                    returned points.
//! \return A ::qdb_error_t code indicating success or failure.
//! \warning This function is still under development. Performance and
//! compatibility are not guaranteed.
//! \see ::qdb_release
QDB_API_LINKAGE qdb_error_t
qdb_ts_blob_get_ranges(qdb_handle_t handle,
                       const char * alias,
                       const char * column,
                       const qdb_ts_range_t * ranges,
                       qdb_size_t range_count,
                       qdb_ts_blob_point ** points,
                       qdb_size_t * point_count);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
