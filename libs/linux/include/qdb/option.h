#ifndef QDB_API_OPTION_H
#define QDB_API_OPTION_H

//! \file option.h
//! \defgroup option Options

/*
 *
 * Official quasardb C API 2.1.0
 * 1a277ed 2017-08-04 15:38:44 +0000
 *
 * Copyright (c) 2009-2017, quasardb SAS
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of quasardb nor the names of its contributors may
 *      be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY QUASARDB AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "client.h"

#ifdef __cplusplus
extern "C" {
#endif

//! \ingroup client
//! \brief Sets the timeout of all network operations.
//! \param handle The qdb_handle_t that was initialized with ::qdb_open or
//! ::qdb_open_tcp
//! \param timeout_ms The timeout of network operation, in milliseconds.
//! \return A ::qdb_error_t code indicating success or failure.
//! \remark The lower the timeout, the higher the risk of having timeout errors.
QDB_API_LINKAGE qdb_error_t qdb_option_set_timeout(qdb_handle_t handle,
                                                   int timeout_ms);

//! \ingroup client
//! \brief Sets the maximum allowed cardinality of a quasardb query. The default
//! value is 100'003. The minimum allowed values is 1,000.
//! \param handle The qdb_handle_t that was initialized with ::qdb_open or
//! ::qdb_open_tcp
//! \param max_cardinality The maximum cardinality of a query.
//! \return A ::qdb_error_t code indicating success or failure.
//! \remark Improper usage of this function may cause a denial of service on the
//! client.
QDB_API_LINKAGE qdb_error_t
qdb_option_set_max_cardinality(qdb_handle_t handle, qdb_uint_t max_cardinality);

//! \ingroup client
//! \enum qdb_compression
//! \brief An enumeration of compression parameters
//! \see qdb_option_set_compression

//! \ingroup client
//! \typedef qdb_compression_t
//! \brief A type alias for enum ::qdb_compression
//! \see qdb_option_set_compression
typedef enum qdb_compression {
    //! No compression
    qdb_comp_none = 0,
    //! Maximum compression speed, potentially minimum compression ratio.
    //! This is currently the default.
    qdb_comp_fast = 1,
    //! Maximum compression ratio, potentially minimum compression speed.
    //! This is currently not implemented.
    qdb_comp_best = 2
} qdb_compression_t;

//! \ingroup client
//! \brief Set the compression level for all future messages emitted by the
//! specified handle. Regardless of this parameter, the API will
//!        be able to read whatever compression the server uses.
//! \param handle The handle on which to set the compression level
//! \param comp_level The compression level to use
//! \return A ::qdb_error_t code indicating success or failure.
//! \see qdb_compression_t
//! \remark For better performance, consider disabling compression if your data
//! is already compressed.
QDB_API_LINKAGE qdb_error_t
qdb_option_set_compression(qdb_handle_t handle, qdb_compression_t comp_level);

typedef enum qdb_encryption {
    qdb_crypt_none = 0,
    qdb_crypt_aes_gcm_256 = 1
} qdb_encryption_t;

QDB_API_LINKAGE qdb_error_t
qdb_option_set_encryption(qdb_handle_t handle, qdb_encryption_t encryption);

QDB_API_LINKAGE qdb_error_t
qdb_option_set_cluster_public_key(qdb_handle_t handle, const char * public_key);

// an user name may not exceed a predefined size
#define qdb_max_user_name_length 120

QDB_API_LINKAGE qdb_error_t
qdb_option_set_user_credentials(qdb_handle_t handle,
                                const char * user_name,
                                const char * private_key);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* QDB_API_OPTION_H */
