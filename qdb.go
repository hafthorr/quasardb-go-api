// Copyright 2017 Hlep Ltd

package qdb

/*
#cgo darwin LDFLAGS: -pthread -L"${SRCDIR}/libs/darwin/lib" -lqdb_api
#cgo darwin CFLAGS: -I"${SRCDIR}/libs/darwin/include"

#cgo linux LDFLAGS: -pthread -L"${SRCDIR}/libs/linux/lib" -lqdb_api
#cgo linux CFLAGS: -I"${SRCDIR}/libs/linux/include"

#include <stdlib.h>
#include <qdb/error.h>
#include <qdb/blob.h>
#include <qdb/client.h>
#include <qdb/prefix.h>
#include <qdb/tag.h>
#include <qdb/stream.h>
#include <qdb/batch.h>

// Wrapper for errors macro
static inline int qdb_success(qdb_error_t e) { return QDB_SUCCESS(e); }
static inline int qdb_failure(qdb_error_t e) { return QDB_FAILURE(e); }
static inline int qdb_error_severity(qdb_error_t e) { return QDB_ERROR_SEVERITY(e); }

// Wrapper to simplify pointer's arithmetic
static inline const char *stringAt(const char **p, size_t idx) { return p[idx]; }

static inline const void *op_get_content(qdb_operation_t *op) { return op->blob_get.content; }
static inline int op_get_content_size(qdb_operation_t *op) { return op->blob_get.content_size; }

static inline void op_update(qdb_operation_t *op, void *data, size_t size)
{
	op->type = qdb_op_blob_update;
	op->blob_update.content = data;
	op->blob_update.content_size = size;
}
static inline void op_get(qdb_operation_t *op, size_t offset)
{
	op->type = qdb_op_blob_get;
	op->blob_get.content_offset = offset;
}

*/
import "C"

import (
	"bytes"
	"errors"
	"io"
	"unsafe"
)

const bufferSize = 1 * 1024 * 1024
const BatchOpsMax = 64

// TODO Consider to use a regular approach var NotFoundErr = errors.New(...)
// Instead of using checker function for each type
type qError struct {
	code C.qdb_error_t
	ctx  string
}

type Batch struct {
	handle C.qdb_handle_t

	count int
	ops   [BatchOpsMax]C.qdb_operation_t
}

type BlobOpType int

const (
	BlobGet    BlobOpType = C.qdb_op_blob_get
	BlobUpdate BlobOpType = C.qdb_op_blob_update
)

type BlobOperation struct {
	Type   BlobOpType
	Alias  string
	Blob   []byte
	Offset int64
	Error  error
}

func (e qError) Error() string {
	return e.ctx + ": " + C.GoString(C.qdb_error(e.code))
}

// IsTagNotSetError returns true is error was caused by absence of tag
func IsTagNotSetError(e error) bool {
	if err, ok := e.(qError); ok {
		if err.code == C.qdb_e_tag_not_set {
			return true
		}
	}
	return false
}

// IsAliasNotFoundError returns true if error denotes that alias was not found
func IsAliasNotFoundError(e error) bool {
	if err, ok := e.(qError); ok {
		if err.code == C.qdb_e_alias_not_found {
			return true
		}
	}
	return false
}

// IsAliasAlreadyExistsError returns true if error denotes that alias was already exsisted at operation time
func IsAliasAlreadyExistsError(e error) bool {
	if err, ok := e.(qError); ok {
		if err.code == C.qdb_e_alias_already_exists {
			return true
		}
	}
	return false
}

// IsBufferTooSmallError returns true if buffer provided for call was too little to hold all data
func IsBufferTooSmallError(e error) bool {
	if err, ok := e.(qError); ok {
		if err.code == C.qdb_e_buffer_too_small {
			return true
		}
	}
	return false
}

// IsUnrecoverable returns true if error cannot be recovered
func IsUnrecoverable(e error) bool {
	if err, ok := e.(qError); ok {
		if C.qdb_error_severity(err.code) == C.qdb_e_severity_unrecoverable {
			return true
		}
	}
	return false
}

type qdbc struct {
	handle  C.qdb_handle_t
	connstr *C.char
}

// New returns new instance of qbd client
func New() (DB, error) {
	c := &qdbc{}

	err := wrapError(C.qdb_open(&c.handle, C.qdb_p_tcp), "open qdb")
	return c, err
}

func (q *qdbc) Connect(hosts ...string) error {
	str := formatHosts(hosts...)
	if len(str) == 0 {
		return errors.New("invalide argument")
	}

	q.connstr = C.CString(str)
	return wrapError(C.qdb_connect(q.handle, q.connstr), "qdb connect")
}

func (q *qdbc) Close() error {
	err := wrapError(C.qdb_close(q.handle), "qdb close")
	C.free(unsafe.Pointer(q.connstr))
	return err
}

func (q *qdbc) Put(alias string, content []byte, expiry ExpiryTime) error {
	pk := C.CString(alias)
	defer C.free(unsafe.Pointer(pk))

	var pv unsafe.Pointer
	if content != nil {
		pv = unsafe.Pointer(&content[0])
	}
	sz := C.qdb_size_t(len(content))

	exp := C.qdb_time_t(expiry)
	return wrapError(C.qdb_blob_put(q.handle, pk, pv, sz, exp), "qdb blob put")
}

// TODO: buffer too small error from qdb_e_buffer_too_small
func (q *qdbc) Get(alias string, content []byte) (int, error) {
	pk := C.CString(alias)
	defer C.free(unsafe.Pointer(pk))

	var pv unsafe.Pointer
	if content != nil {
		pv = unsafe.Pointer(&content[0])
	}
	sz := C.qdb_size_t(len(content))

	err := wrapError(C.qdb_blob_get_noalloc(q.handle, pk, pv, &sz), "qdb blob get")
	if err != nil {
		return int(sz), err
	}

	if int(sz) > len(content) {
		return int(sz), errors.New("buffer is too small")
	}

	return int(sz), nil
}

func (q *qdbc) Remove(alias string) error {
	pk := C.CString(alias)
	defer C.free(unsafe.Pointer(pk))

	return wrapError(C.qdb_remove(q.handle, pk), "qdb remove")
}

func (q *qdbc) Update(alias string, content []byte, expiry ExpiryTime) error {
	pk := C.CString(alias)
	defer C.free(unsafe.Pointer(pk))

	var pv unsafe.Pointer
	if content != nil {
		pv = unsafe.Pointer(&content[0])
	}
	sz := C.qdb_size_t(len(content))

	exp := C.qdb_time_t(expiry)
	ret := C.qdb_blob_update(q.handle, pk, pv, sz, exp)

	// TODO: Workaround - blob_update could return nonzero error code for valid operation
	// For instance, qdb_e_ok_created when blob created by update
	if C.qdb_success(ret) != 0 {
		return nil
	}

	return wrapError(ret, "qdb blob update")
}

func (q *qdbc) Version() string {
	return C.GoString(C.qdb_version())
}

func (q *qdbc) Build() string {
	return C.GoString(C.qdb_build())
}

func (q *qdbc) Prefix(prefix string, max int) ([]string, error) {
	var sz C.size_t
	var pres **C.char
	var m C.qdb_int_t = C.qdb_int_t(max)

	px := C.CString(prefix)
	defer C.free(unsafe.Pointer(px))

	err := wrapError(C.qdb_prefix_get(q.handle, px, m, &pres, (*C.size_t)(&sz)), "qdb prefix get")
	if err != nil {
		return nil, err
	}
	defer C.qdb_release(q.handle, unsafe.Pointer(pres))

	if max < int(sz) {
		return nil, errors.New("internal qbd error")
	}

	// TODO Consider to pass already allocated slice and use its size as max in the call above
	res := make([]string, 0, int(sz))
	for i := C.size_t(0); i < sz; i++ {
		res = append(res, C.GoString(C.stringAt(pres, i)))
	}

	return res, nil
}

func (q *qdbc) AttachTag(alias, tag string) error {
	pk := C.CString(alias)
	pt := C.CString(tag)
	defer C.free(unsafe.Pointer(pk))
	defer C.free(unsafe.Pointer(pt))

	return wrapError(C.qdb_attach_tag(q.handle, pk, pt), "qdb attach tag")
}

func (q *qdbc) DetachTag(alias, tag string) error {
	pk := C.CString(alias)
	pt := C.CString(tag)
	defer C.free(unsafe.Pointer(pk))
	defer C.free(unsafe.Pointer(pt))

	return wrapError(C.qdb_detach_tag(q.handle, pk, pt), "qdb detach tag")
}

func (q *qdbc) HasTag(alias, tag string) error {
	pk := C.CString(alias)
	pt := C.CString(tag)
	defer C.free(unsafe.Pointer(pk))
	defer C.free(unsafe.Pointer(pt))

	return wrapError(C.qdb_has_tag(q.handle, pk, pt), "qdb has tag")
}

func (q *qdbc) Write(alias string, r io.Reader) (int64, error) {
	pk := C.CString(alias)
	defer C.free(unsafe.Pointer(pk))

	var stream C.qdb_stream_t
	err := wrapError(C.qdb_stream_open(q.handle, pk, C.qdb_stream_mode_append, &stream), "qdb stream")
	if err != nil {
		return 0, err
	}
	defer C.qdb_stream_close(stream)

	// This is quite important, because we could re-use the same alias
	// And double write with same key without truncating could lead to unexpected results
	err = wrapError(C.qdb_stream_truncate(stream, C.qdb_stream_size_t(0)), "qdb truncate")
	if err != nil {
		return 0, err
	}

	// TODO: Would be nice to have some "shared buffers", so we don;t have to
	// allocate it on the stack ?
	var buffer [bufferSize]byte

	total := int64(0)
	for {
		var rd int
		rd, err = r.Read(buffer[:])
		if rd == 0 {
			if err == io.EOF {
				return total, nil
			}
			break
		}

		err = wrapError(C.qdb_stream_write(stream, unsafe.Pointer(&buffer[0]), C.size_t(rd)), "qdb stream write")
		if err != nil {
			break
		}

		total += int64(rd)
	}

	return total, err
}

func (q *qdbc) Read(alias string, offset, length int64, w io.Writer) (int64, error) {
	if length < 0 || offset < 0 {
		return 0, wrapError(C.qdb_e_invalid_argument, "qdb stream read")
	}

	pk := C.CString(alias)
	defer C.free(unsafe.Pointer(pk))

	var stream C.qdb_stream_t
	err := wrapError(C.qdb_stream_open(q.handle, pk, C.qdb_stream_mode_read, &stream), "qdb stream")
	if err != nil {
		return 0, err
	}
	defer C.qdb_stream_close(stream)

	if offset > 0 {
		err := wrapError(C.qdb_stream_setpos(stream, C.qdb_stream_size_t(offset)), "qdb stream pos")
		if err != nil {
			return 0, err
		}
	}

	// TODO: Would be nice to have some "shared buffers", so we don;t have to
	// allocate it on the stack ?
	var buffer [bufferSize]byte

	total := int64(0)
	for {
		var read C.size_t = C.size_t(length - total)

		if read > bufferSize || length == 0 {
			read = bufferSize
		}

		err = wrapError(C.qdb_stream_read(stream, unsafe.Pointer(&buffer[0]), &read), "qdb stream read")
		if err != nil || read == 0 {
			break
		}

		var wr int
		wr, err = w.Write(buffer[:read])
		if err != nil {
			break
		}

		total += int64(wr)
	}

	return total, err
}

func (q *qdbc) BlobBatch(ops ...BlobOperation) (int, error) {
	opscnt := C.size_t(len(ops))
	cops := make([]C.qdb_operation_t, len(ops))

	var pcops *C.qdb_operation_t = (*C.qdb_operation_t)(unsafe.Pointer(&cops[0]))

	err := wrapError(C.qdb_init_operations(pcops, opscnt), "qdb init batch")
	if err != nil {
		return 0, err
	}

	for i, op := range ops {
		cop := &cops[i]
		cop.alias = C.CString(op.Alias)

		switch op.Type {
		case BlobUpdate:
			var p unsafe.Pointer
			if op.Blob != nil {
				p = unsafe.Pointer(&op.Blob[0])
			}
			C.op_update(cop, p, C.size_t(len(op.Blob)))

		case BlobGet:
			C.op_get(cop, C.size_t(op.Offset))
		}
	}

	var cnt C.qdb_size_t = C.qdb_run_batch(q.handle, pcops, opscnt)

	for i := range ops {
		cop := &cops[i]
		C.free(unsafe.Pointer(cop.alias))
		ops[i].Error = wrapError(cop.error, "qdb batch io")

		if ops[i].Type == BlobGet && ops[i].Error == nil {
			ops[i].Blob = C.GoBytes(C.op_get_content(cop), C.op_get_content_size(cop))
		}
	}

	C.qdb_release(q.handle, unsafe.Pointer(pcops))
	return int(cnt), err
}

// concat hosts list to one comma separated string
func formatHosts(hosts ...string) string {
	if len(hosts) == 0 {
		return ""
	}
	buf := &bytes.Buffer{}
	buf.WriteString(hosts[0])
	for _, s := range hosts[1:] {
		buf.WriteString(",")
		buf.WriteString(s)
	}

	return buf.String()
}

func wrapError(e C.qdb_error_t, msg string) error {
	// It's impossible to use qdb_success / qdb_failure here
	// since some conditions I'd like treat as error
	// For instance, tag not found or alias not found are count as info severity
	if e == 0 || e == C.qdb_e_ok_created {
		return nil
	}

	return qError{
		code: e,
		ctx:  msg,
	}
}
