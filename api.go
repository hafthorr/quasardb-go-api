// Copyright 2017 Hlep Ltd

package qdb

import "io"

// DB is interface for quasardb
type DB interface {
	// Connect binds the client instance to a quasardb cluster and connect to at least one node within.
	// Quasardb URI are in the form qdb://<address>:<port> where <address> is either an IPv4 or IPv6
	// (surrounded with square brackets), or a domain name.
	// It is recommended to specify multiple addresses should the designated node be unavailable.
	//
	// URI examples:
	// qdb://myserver.org:2836 - Connects to myserver.org on the port 2836
	// qdb://127.0.0.1:2836 - Connects to the local IPv4 loopback on the port 2836
	// qdb://[::1]:2836 - Connects to the local IPv6 loopback on the port 2836
	Connect(hosts ...string) error

	// Close closes the handle previously opened with qdb_open or qdb_open_tcp.
	// This results in terminating all connections and releasing all internal buffers,
	// including buffers which may have been allocated as or a result of batch operations or get operations.
	Close() error

	// Sysinfo
	// =======

	// Version returns a string describing the API version.
	Version() string

	// Build returns a string describing the exact API build.
	Build() string

	// CRUD operations set
	// ===================

	// Put creates a new entry and sets its content to the provided blob.
	// If the entry already exists the function will fail and will return error which can be checked by IsAliasAlreadyExistsError.
	// You can specify an expiry or use NeverExpires if you don’t want the entry to expire.
	// There is no software-defined limit to the size of blobs.
	// To operate on entries that do not fit in RAM, consider using streams.
	Put(alias string, content []byte, expiry ExpiryTime) error

	// Get retrieves an entry’s content from the quasardb server.
	// The caller is responsible for allocating and freeing the provided buffer.
	// If the entry does not exist, the function will fail and return error which can be checked by IsAliasNotFoundError.
	// If the buffer is not large enough to hold the data, the function will fail and return error which could be checked
	// with IsBufferTooSmallError.
	// Along with error it will nevertheless return entry size so that the caller may resize its buffer and try again.
	Get(alias string, content []byte) (int, error)

	// Update creates or updates an entry and sets its content to the provided blob.
	// If the entry already exists, the function will modify the entry.
	// You can specify an expiry or use NeverExpires if you don’t want the entry to expire.
	// There is no software-defined limit to the size of blobs.
	// To operate on entries that do not fit in RAM, consider using streams.
	Update(alias string, content []byte, expiry ExpiryTime) error

	// Remove removes an entry from the cluster, regardless of its type.
	// This call will remove the entry, whether it is a blob, integer, deque, stream or hset.
	// It will properly untag the entry.
	// If the entry spawns on multiple entries or nodes (deques, hsets and streams) all blocks will be properly removed.
	// The call is ACID, regardless of the type of the entry and a transaction will be created if need be.
	// The function fails if the entry does not exist.
	Remove(alias string) error

	// Search and query operation set
	// ==============================

	// Prefix retrieves the list of all entries matching the provided prefix.
	// A prefix-based search will enable you to find all entries matching a provided prefix.
	// This function returns the list of aliases.
	// It’s up to the user to query the content associated with every entry, if needed.
	Prefix(prefix string, max int) ([]string, error)

	// Adds a tag to an entry.
	// Tagging an entry enables you to search for entries based on their tags. Tags scale across nodes.
	// The entry must exist.
	// The tag may or may not exist.
	// Consider using AttachTags if you are adding several tags to the same entry.
	AttachTag(alias, tag string) error

	// Removes a tag from an entry.
	// Tagging an entry enables you to search for entries based on their tags. Tags scale across nodes.
	// The entry must exist.
	// The tag must exist.
	// Consider using DetachTags if you are removing several tags at once.
	DetachTag(alias, tag string) error

	// Tests if an entry has the request tag.
	// Tagging an entry enables you to search for entries based on their tags. Tags scale across nodes.
	// The entry must exist.
	// If you need to test several entries and/or several tags, consider using a batch for maximum performance.
	HasTag(alias, tag string) error

	// Write provides way to put value to DB from the io.Reader interface
	Write(alias string, r io.Reader) (int64, error)

	// Read provides way to get value from DB to the io.Writer interface
	// The offset specify offset in the value to start with (cannot be negative)
	// The lenght specify amount of bytes needs to be read (zero value means until the end)
	Read(alias string, offset, length int64, w io.Writer) (int64, error)

	// Run batch operations
	BlobBatch(ops ...BlobOperation) (int, error)
}
