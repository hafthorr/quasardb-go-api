// Copyright 2017 Hlep Ltd

package qdb

import "time"

// ExpiryTime - the absolute expiration time is the Unix epoch, that is,
// the number of milliseconds since 1 January 1970, 00:00::00 UTC.
type ExpiryTime int64

// NeverExpires is an arbitrary time value representing the “no expiration” time value.
const NeverExpires = ExpiryTime(0)

// Convert nanoseconds to milliseconds
const nsec2ms int64 = 1000000

// ExpiryFromNow returns relative expire time value
func ExpiryFromNow(d time.Duration) ExpiryTime {
	return ExpiryTime(time.Now().Add(d).UnixNano() / nsec2ms)
}

// ExpiryAt return absolute expire time value
func ExpiryAt(t time.Time) ExpiryTime {
	return ExpiryTime(t.UnixNano() / nsec2ms)
}
