# Go wrapper for Quasar DB

### Design and implementation notes

Why is DB expose as an interface instead of returning some client struct ?
There are at least three reasons for that:
- I didn't look up how to do this wrapper on Windows platform and it could be something different than simple cgo calls.
- If quasardb publish protocol for API it'd be easy to remove cgo bindings and implement that protocol without changing clients.  
- Separate interface with doc-strings from actual code to keep it in small different files.

Why there is no any exported `errors values` ?
Because it'd require more effort to support them. Instead `qdb` package exports several functions to check specific
errors without exporting underlying types (which may changes soon according to quasardb docs).

### Examples

TBA

### Written by 

DenisB <3
